/*External JS*/

let myGrades = [98, 95, 75, 99, 100]; //array

/* object = collection of data or functionality
 			= which consist of several variable and functions
			=which are called properties and methods when they are objects
 */

 /*
	cellphone - an object 
	 properties (variables)
	  -color
	  -weight
	  -unit/model
	  -brand

	functions (methods)
	 - alarm
	 -open
	 -close
	 -ringing

 */

 // creating an object
 let cellphone = {
 	color: 'black',
 	weight: '115 grams',
 	model: 'iPhone13',
 	brand: 'iPhone',
 	ring: function(){
 		console.log('cellphone');
 	}
 };

 console.log(cellphone);


 //method 2
 let car = Object();
 car.color = 'red';
 car.model = 'vios 2021';
 car.drive = function(){
 	console.log('car is running')
 };

 console.log(car);


 /*Accessing properties and methds of an object 
  -to access the properties and methods we will use dot notations.

  syntax: 
     object.properties or object['properties']
     object.methods()

 */

/*
	Re-assigning/Editing 
	 - to re-assign values of properties and methods just use the assignment operator (=)

	ex. cellphone.color ='blue'

*/


cellphone.color = 'blue';
console.log(cellphone.color);

cellphone.ring = function() {
	console.log('the blue cellphone is ringing.')
};

console.log(cellphone.ring());

/*
	to delete properties/methods of an object use the keyword "delete".

	ex. delete cellphone.color
		delete cellphone.ring
*/

delete cellphone.color;
delete cellphone.ring;
console.log(cellphone);

/* object constructor 

Case: 
	imagine we are going to create a pokemon with multiple properties. 
		-name
		-level
		-health
		-attack
		-element
		-action

*/

// let pokemon = {
// 	name : 'Pikachu',
// 	level : 3,
// 	health : 100,
// 	attack : 50,
// 	element : 'electric',
// 	action :function(){
// 		console.log("This pokemon attacked Charmander");
// 		console.log("Charmander's health is now reduced");
// 	}

// };

// console.log(pokemon);

/* Object constructor -
	a function that initializes an object 

*/
//object contructor
// function Pokemon(name, level, element) {
// 	this.name = name;
// 	this.level = level;
// 	this.element = element;
// 	this.health = 100 + (2* level);
// 	this.attack = 50;
// 	this.action = function() {
// 		console.log(this.name + "tackled targetPokemon");
// 		console.log("Charmander's health is now reduced");
	
// 	};
// 	this.die = function() {
// 		console.log(this.name +'died')
// 	}

// };

// let pikachu = new Pokemon('pikachu', 16, 'electric');
// console.log(pikachu);

// let ratata = new Pokemon('ratata', 16, 'ground');
// console.log(ratata);

function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`)
		target.health -=10;
		console.log(`${target.name}'s health is now ${target.health}`)
	}
};

let Pikachu = new Pokemon('Pikachu');
let Geodude = new Pokemon('Geodude');

Pikachu.attack(Geodude);

console.log('=== mini activity ===');



